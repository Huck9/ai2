<?php

namespace App\Controller;

use App\Entity\City;
use App\Repository\CityRepository;
use App\Repository\MeasurementRepository;
use App\Service\WeatherUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class WeatherController extends AbstractController
{

    public function cityAction(string $country, string $city, WeatherUtil $weatherUtil): Response
    {
        $measurement = $weatherUtil->getWeatherForCountryAndCity($country, $city);
        return $this->render('weather/city.html.twig', [
            'city' => $city,
            'country' => $country,
            'measurements' => $measurement,
        ]);
    }
}
