<?php

namespace App\Service;

use App\Entity\City;
use App\Entity\Measurement;
use App\Repository\CityRepository;
use App\Repository\MeasurementRepository;
use Symfony\Component\HttpFoundation\Response;

class WeatherUtil

{
    private $measurementRepository;
    private $cityRepository;

    public function __construct(MeasurementRepository $measurementRepository, CityRepository $cityRepository){
        $this->measurementRepository = $measurementRepository;
        $this->cityRepository = $cityRepository;
    }

    public function getWeatherForCountryAndCity(string $country, string $city): array
    {
        $city = $this->cityRepository->findOneBy(['country' => $country, 'name' => $city]);
        return $this->getWeatherForLocation($city);
    }

    public function getWeatherForLocation(City $city): array
    {
        return $this->measurementRepository->findByLocation($city);
    }


    public function getWeatherForLocationId($cityId): array
    {
        $city = $this->cityRepository->find($cityId);
        return $this->measurementRepository->findByLocation($city);
    }
}