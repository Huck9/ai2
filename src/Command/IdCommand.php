<?php

namespace App\Command;

use App\Service\WeatherUtil;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class IdCommand extends Command
{
    protected static $defaultName = 'app:id';
    protected static $defaultDescription = 'Add a short description for your command';

    private $weatherUtil;

    public function __construct(WeatherUtil $weatherUtil)
    {
        $this->weatherUtil = $weatherUtil;

        parent::__construct();
    }


    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('cityId', InputArgument::REQUIRED, 'City ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument('cityId');
        $measurments = $this->weatherUtil->getWeatherForLocationId($id);
        $output->writeln($measurments[0]->getCity()->getName());
        foreach ($measurments as $measurment){
            $output->writeln($measurment->getDate()->format('Y-m-d'). " - ".$measurment->getTemperature() . "°C");
        }
        return 0;
    }
}
